# Invisible Island
## Devices Deployment Kit

```
Version :  0.2.0
URL     :  https://bitbucket.org/locative/invisibleislands-devices
Git     :  git@bitbucket.org:locative/invisibleislands-devices.git
```

The device deployment kit supports the following hardware:

- Iteration M0 (August 2014): Raspberry Pi Model B, Edimax EW-7811Un WiFi
- Iteration M1 (April 2015): Raspberry Pi Model B+, AusPi WiFi
- Iteration M2 (August 2015): Raspberry Pi 2+, TL-WN722

You'll need to download the [latest Raspbian](https://www.raspberrypi.org/downloads/)
image to get started and plug the SD-Card into your computer.

The kit offers three scripts:

- `raspberry-format.py` will flash the Raspbian image onto the SD-Card connected to your computer.

- `raspberry-setup.py` will configure th Raspberry Pi, installaing all
  the required packages, groups, users, etc, so that the Island software
  is ready to be installed. This will require the device to be 
  network-accessible.

These scripts require Python 2.7+ and the `cuisine` module.

errors

# TODO: Fix
"""
Fatal error: sudo() received nonzero return code 100 while executing!

Requested: DEBIAN_FRONTEND=noninteractive apt-get -q --yes -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install python-dev
Executed: sudo -S -p 'sudo password:'  /bin/bash -l -c "DEBIAN_FRONTEND=noninteractive apt-get -q --yes -o Dpkg::Options::=\"--force-confdef\" -o Dpkg::Options::=\"--force-confold\" install python-dev"

Aborting.
sudo() received nonzero return code 100 while executing!

Requested: DEBIAN_FRONTEND=noninteractive apt-get -q --yes -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" install python-dev
Executed: sudo -S -p 'sudo password:'  /bin/bash -l -c "DEBIAN_FRONTEND=noninteractive apt-get -q --yes -o Dpkg::Options::=\"--force-confdef\" -o Dpkg::Options::=\"--force-confold\" install python-dev"
"""
# EOF
