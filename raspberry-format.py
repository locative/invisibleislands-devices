#!/usr/bin/env python
import os, sys, glob, cuisine

__doc__ = """
A command-line script that takes care of flashing the Raspbian image to
the computer's SD card. It basically automates the process described
on http://www.raspberrypi.org/documentation/installation/installing-images/README.md

This script requires Linux.
"""

VERSION           = "0.0.1"
ISLAND_DISTRO     = "????-??-??-raspbian*.img"
ISLAND_DISTRO_URL = "http://downloads.raspberrypi.org/raspbian_latest"
SEARCH_PATH       = (".", "~/Desktop", "~/Downloads", "~")

def search_images():
	res = []
	for d in SEARCH_PATH:
		p    =  os.path.join(os.path.abspath(os.path.expanduser(d)), ISLAND_DISTRO)
		res += glob.glob(p)
	return sorted(res, lambda a,b:cmp(a.rsplit("/",1)[-1], b.rsplit("/",1)[-1]))

def run( args ):
	images   = search_images()
	img_path = args[0] if len(args) > 0 else (images and images[0] or None)
	out      = cuisine.log_message
	err      = cuisine.log_error
	cuisine.mode_local()

	out ("Invisible Island - Island Bootstrapping Tool v.{0}".format(VERSION))

	# We check for the Raspbian disk image
	if not img_path or not os.path.exists(img_path):
		err ("[!] Raspbian disk image expected")
		out ("    Get it from {0}".format(ISLAND_DISTRO_URL))
		out ("    and pass it as first argument to this script")
		out ("--> {0} {1}".format(sys.argv[0], ISLAND_DISTRO))
		sys.exit(-1)

	# We list the MMCBLK devices
	devices = sorted(glob.glob("/dev/mmcblk?"))
	if len(devices) == 0:
		err("[!] No SD-card block device detected, you should have at /dev/mmcblk0")
		sys.exit(-1)
	if len(devices) != 1:
		err("[!] Only one input device is supported")
		sys.exit(-1)

	# We check for sudo
	has_sudo = os.popen("sudo echo OK").read()
	if has_sudo != "OK\n":
		err("[!] Administrator privileges are required to flash the block device")
		sys.exit(-1)

	device = devices[0]
	out ("Preparing to Flash device {0}, this will take about between 5 and 15 minutes".format(device))
	cmd = "dd bs=1M if={0} of={1}".format(img_path, device)
	out ("--> {0}".format(cmd))
	result = cuisine.sudo(cmd)

	# We ensure that the data is flushed
	out ("Flushing filesystem buffers (this might take a few seconds)")
	res = cuisine.sudo("sync")
	out (cuisine.text_strip_margin("""\n
	|All done!

	|You'll need:

	|- 1 Raspbery Pi
	|- 1 Mini-USB cable to power the Raspberry Pi
	|- 1 Edimax EW-7811UN WiFi dongle
	|- 1 HDMI monitor & cable
	|- 1 USB keyboard
	|- 1 Ethernet cables

	|Now:

	|- Unplug your Raspberry Pi if it was plugged
	|- Connect it to a screen and keyboard
	|- Remove any other USB connection, especially WiFi dongles
	|- Insert the SD card
	|- Plug USB power into
	|- The Raspberry will boot and you'll see text logs on the screen
	|- Configure Raspbian, by expanding the SD card to make it full
	|  size and making sure "raspberry" is the default password for the "pi" user.ping
	|- Plug the Raspberry Pi into your local network (use the Ethernet
	|  cable and plug it into your WiFi router or your LAN ethernet ports).
	|- Plug the Edimax WiFi dongle
	|- Reboot your Raspberry PI

	|You should now see a message "My IP address is XXX.XXX.XXX.XXX". If not,
	|login (user is `pi`, password is `raspberry`) and type

	|$ ifconfig eth0

	|You should see  something like

	|eth0      Link encap:Ethernet  HWaddr 70:11:24:8d:da:ec
	|		  inet addr:10.11.98.222  Bcast:10.11.127.255  Mask:255.255.192.0
	|		  inet6 addr: fe80::7211:24ff:fe8d:daec/64 Scope:Link
	|		  UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
	|		  RX packets:1186267 errors:0 dropped:0 overruns:0 frame:0
	|		  TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
	|		  collisions:0 txqueuelen:1000
	|		  RX bytes:294103810 (294.1 MB)  TX bytes:14360817 (14.3 MB)

	|Note the IP address after the "inet addr:", in this case "10.11.98.222".

	|You are now ready to run the 'raspberrypi-setup.py' script with the given
	|IP address."""))

if __name__ == "__main__":
	run(sys.argv[1:])

# EOF
