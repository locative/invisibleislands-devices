#!/usr/bin/env python
# encoding=utf8 ---------------------------------------------------------------
# Project           : Invisible Islands
# -----------------------------------------------------------------------------
# Author            : Locative
# License           : BSD License
# -----------------------------------------------------------------------------
# Creation date     : 2014-08-01
# Last modification : 2015-07-20
# -----------------------------------------------------------------------------

VERSION = "0.0.0"
LICENSE = "http://ffctn.com/doc/licenses/bsd"

import os, sys, glob

__doc__ = """
Sets up a Rapsberry Pi reachable through the local network to be ready
to run an Island.
"""

try:
	import cuisine
except ImportError as  e:
	print ("Python package `cuisine` required")
	sys.exit(-1)

# SEE: https://groups.google.com/forum/#!topic/beagleboard/0T1ia0F9TsM
# ATHEROS CHIPSET WIFI
# wpa_supplicant password > /etc/wpa-wifi.conf
# auto wlan0
# iface wlan0 inet dhcp
# wpa-driver wext
# wpa-conf /etc/wpa_supplicant.conf
# ifdown wlan0
# ifup wlan0

#
# -----------------------------------------------------------------------------
#
# GLOBALS
#
# -----------------------------------------------------------------------------

WIFI_DONGLES = {
	"EW-7811Un" : ("Realtek RTL8188CUS",),
	"AusPI"     : ("RTL8191SU",),
	"TL-WN722N" : ("Atheros Communications, Inc. AR9271 802.11n",),
}

PACKAGE_REMOVE = (
	"lightdm",
	"xserver-xorg",
	"xserver-common",
	"desktop-base",
	"apache2",
)

PACKAGE_ENSURE = (
	"build-essential",
	"tmux",
	"libev-dev",
	"python-crypto",
	"python-gevent",
	"python-setuptools",
	"python-dev",
)

# -----------------------------------------------------------------------------
#
# FUNCTIONS
#
# -----------------------------------------------------------------------------

def detect_features(features):
	features ["wifi"] = detect_wifi()
	return features

def detect_wifi():
	"""Detects the WiFi dongle, returning its product code."""
	lsusb = cuisine.sudo("lsusb").split("\n")
	for device in WIFI_DONGLES:
		for p in WIFI_DONGLES[device]:
			for line in lsusb:
				if line.find(p) >= 0:
					return device
	return None

def clean_iptables(t=None):
	"""Removes the rule from iptables."""
	t = t or cuisine.sudo("iptables -L -t nat --line-numbers")
	res = []
	# FIXME: Fabric does DOS EOLs...
	for block in t.split("\r\n\r\n"):
		if not block: continue
		lines = block.split("\n")
		chain = lines[0].split()
		if len(chain) > 1:
			chain = chain[1]
		else:
			continue
		for rule in lines[2:]:
			if not rule: continue
			rule = rule.split()
			if not len(rule) > 1: continue
			rule = rule[0]
			if rule in ("Chain", "num"): continue
			res.append((chain, rule))
	res.reverse()
	for chain, rule in res:
		cuisine.sudo("iptables -t nat -D {0} {1}".format(chain, rule))

def install_hostapd( features ):
	# The next step is to upload the package
	if features["wifi"] == "EW-7811Un":
		hostapd_targz = "RTL8188-hostapd-1.1.tar.gz"
		hostapd_src   = "RTL8188-hostapd-1.1"
		# We upload the package if necessary
		if not cuisine.file_exists("~/" + hostapd_targz):
			cuisine.file_upload("~", "files/" + hostapd_targz)
		# We unpack it if necessary
		if not cuisine.dir_exists("~/" + hostapd_src):
			cuisine.run("tar fvxz ~/" + hostapd_targz)
		# Then we build hostapd
		if not cuisine.file_exists(hostapd_src + "/hostapd/hostapd"):
			with cuisine.cd(hostapd_src + "/hostapd"):
				cuisine.run("make")
		# If hostapd is not installed, we install it
		if not cuisine.command_check("hostapd"):
			with cuisine.cd(hostapd_src + "/hostapd"):
				cuisine.sudo("make install")
			assert cuisine.command_check("hostapd")
	else:
		cuisine.sudo("apt-get install hostapd")
		assert cuisine.command_check("hostapd")

def setup_hostapd(features):
	"""Sets up the hostapd configuration."""
	if not cuisine.command_check("hostapd"): install_hostapd(features)
	#  We update the hostapd conf file
	with cuisine.mode_sudo():
		suffix = features["wifi"] == "EW-7811Un" and "EW-7811Un" or ""
		hostapd_conf = cuisine.file_local_read("files/etc/hostapd/hostapd{0}.conf".format(suffix))
		hostapd_conf = cuisine.text_template(hostapd_conf, dict(SSID=features.get("SSID") or "Island"))
		cuisine.file_write("/etc/hostapd/hostapd.conf", hostapd_conf)
		cuisine.run("service hostapd restart")

def setup_captive():
	"""Sets up the captive portal mode. This cleans up IP table and sets up
	the redirection rules."""
	with cuisine.mode_sudo():
		ip        = "10.0.0.1"
		interface = "wlan0"
		# FROM: http://unix.stackexchange.com/questions/132130/iptables-based-redirect-captive-portal-style
		cuisine.run("iptables -t nat -A PREROUTING  -i {1} -p tcp --dport 80  -j DNAT --to-destination {0}:80".format(ip, interface))
		cuisine.run("iptables -t nat -A PREROUTING  -i {1} -p tcp --dport 430 -j DNAT --to-destination {0}:80".format(ip,interface))
		cuisine.run("iptables -t nat -A POSTROUTING -j MASQUERADE")
		config = cuisine.run("sudo iptables -L -t nat --line-numbers")
		print (config)

def setup_dnsmasq(features):
	"""Sets up the DNSmasq configuration."""
	# We ensure dnsmasq
	cuisine.package_ensure("dnsmasq")
	with cuisine.mode_sudo():
		cuisine.file_write("/etc/dnsmasq.conf", cuisine.file_local_read("files/etc/dnsmasq.conf"))
		cuisine.file_write("/etc/dnsmasq.d/catchall.conf", cuisine.file_local_read("files/etc/dnsmasq.d/catchall.conf"))

def setup_users(features):
	# We register the local user SSH key so that we can login without password
	cuisine.ssh_keygen("pi")
	cuisine.ssh_authorize("pi", cuisine.file_local_read("~/.ssh/id_dsa.pub"))
	# We create the Island users
	with cuisine.mode_sudo():
		cuisine.user_passwd("pi", features["PI_PASSWD"])
		cuisine.group_ensure("webapp")
		cuisine.user_ensure("manage")
		cuisine.ssh_keygen("manage")
		cuisine.ssh_authorize("manage", cuisine.file_local_read("~/.ssh/id_dsa.pub"))
		cuisine.user_ensure("island")
		cuisine.ssh_keygen("island")
		cuisine.ssh_authorize("island", cuisine.file_local_read("~/.ssh/id_dsa.pub"))
		cuisine.group_user_ensure("webapp","island")
		cuisine.dir_ensure("/opt/apps", owner="manage")
		cuisine.dir_ensure("/opt/apps/island", owner="island", group="manage", recursive=True )

def setup_authbind(features):
	"""Sets up authbind to allow webapp group to bind to port 80."""
	# We make sure authbind is setup
	with cuisine.mode_sudo():
		cuisine.package_ensure("authbind")
		cuisine.file_ensure("/etc/authbind/byport/80", owner="manage", group="webapp", mode="550")

def setup_island( features ):
	"""Sets up the Island web application"""
	# We replace the hostname and register the hosts
	with cuisine.mode_sudo():
		cuisine.sudo("easy_install bjoern")
		# SEE: http://ariandy1.wordpress.com/2013/04/07/setting-up-wifi-access-point-with-edimax-ew-7811un-on-raspberry-pi/
		# SEE: http://elinux.org/RPI-Wireless-Hotspot
		# We setup the captive portal interface
		cuisine.file_write ('/etc/hostname', "island\n")
		cuisine.file_upload("/etc", "files/island-iptables")
		cuisine.file_write ('/etc/network/interfaces', cuisine.file_local_read("files/etc/network/interfaces"))
		cuisine.file_update('/etc/rc.local', lambda _:cuisine.text_ensure_line(_,"ifup wlan0"))
		cuisine.file_update('/etc/sysctl.conf', lambda _:cuisine.text_ensure_line(_,"net.ipv4.ip_forward=1"))
		cuisine.file_update('/etc/hosts', lambda _:cuisine.text_ensure_line(_,
			"127.0.1.1	island islands island.local islands.local"
		))
		cuisine.file_update('/etc/init.d/hostapd', lambda _:cuisine.text_replace_line(_,
			"DAEMON_CONF=",
			"DAEMON_CONF=/etc/hostapd/hostapd.conf"
		)[0])
		cuisine.upstart_restart("hostapd")
		cuisine.upstart_restart("dnsmasq")
		# FROM: http://elinux.org/RPI-Wireless-Hotspot
		cuisine.sudo("sudo update-rc.d hostapd defaults")
		cuisine.sudo("sudo update-rc.d dnsmasq defaults")

# -----------------------------------------------------------------------------
#
# RUN (MAIN FUNCTION)
#
# -----------------------------------------------------------------------------

def run( args ):

	# =========================================================================
	# CONNECTION
	# =========================================================================

	if len(args) == 0:
		print ("Usage: {0} IP <PASSWORD>".format(sys.argv[0]))
		sys.exit(-1)
	host     = args[0]
	password = args[0] if len(args) > 1 else "raspberry"
	user     = "pi"

	cuisine.connect(host, user, password)
	assert cuisine.run("echo OK"), "Cannot establish connection to host {0} with user {1} and password {2}".format(host, user, password)

	# =========================================================================
	# FEATURES
	# =========================================================================

	features = {
		"SSID":"îles invisibles",
		# hashlib.sha256("invisibleislands").hexdigest()
		"PI_PASSWD":"44d999c3fc0f9ff68392e4618ede05b6233caac6f86a9696d17e4476e87a37ed"
	}
	features = detect_features(features)

	# =========================================================================
	# PACKAGES
	# =========================================================================
	# We're now logged into the raspberry pi
	# The first step is to cleanup all the junk that is already installed -- and we should remove more than that!
	# cuisine.apt_update()
	for _ in PACKAGE_REMOVE: cuisine.package_clean_apt(_)
	for _ in PACKAGE_ENSURE: cuisine.package_ensure(_)
	cuisine.apt_get("autoremove")

	# =========================================================================
	# SERVICES
	# =========================================================================
	setup_hostapd(features)
	setup_users(features)
	setup_authbind(features)
	setup_dnsmasq(features)
	setup_island(features)

if __name__ == "__main__":
	run(sys.argv[1:])

# EOF - vim: ts=4 sw=4 noet
