#!/usr/bin/env python
import os, sys, glob, json, re

__doc__ = """
Deploys the Island application on the Raspberry PI
"""

# -----------------------------------------------------------------------------
#
# BASIC CONNECTION
#
# -----------------------------------------------------------------------------

try:
	import cuisine
except ImportError as  e:
	print ("Python package `cuisine` required")
	sys.exit(-1)

VERSION  = "0.0.3"
args     = sys.argv[1:]
if len(args) == 0:
	print ("Usage: {0} IP <PASSWORD>".format(sys.argv[0]))
	sys.exit(-1)

host     = args[0]
password = args[0] if len(args) > 1 else "raspberry"

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SCRIPTS_PATH = os.path.join(BASE_PATH, "Scripts")

# -----------------------------------------------------------------------------
#
# UPLOADING THE DISTRIBUTION
#
# -----------------------------------------------------------------------------

# We start by rsync'ing the distribution content to the Raspbery
cuisine.connect(host, "island")
distribution_path = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "Distribution")
assert cuisine.run("echo OK"), "Cannot establish connection to host {0} with user island".format(host)
cmd = "rsync -avz --exclude Data --exclude '*.bak' {0}/ island@{1}:/opt/apps/island/".format(distribution_path, host)
print (cmd)
res_in, res_out, res_err = os.popen3(cmd)
res_out = res_out.read()
res_err = res_err.read()
if "WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!" in res_err:
	for line in [_ for _ in res_err.split("\r\n") if "remove with: ssh-keygen -f " in _]:
		subcmd = line.split(":",1)[-1]
		print ("Cleaning up",subcmd, (os.popen(subcmd).read()))
	print (os.popen(cmd).read())
else:
	print (res_out)

cuisine.file_write("/opt/apps/island/island.json", json.dumps(dict(type="island")))

# -----------------------------------------------------------------------------
#
# SETTING UP THE UPSTART SCRIPT
#
# -----------------------------------------------------------------------------

# We list the running processes and kill any island running
cuisine.connect(host, "pi", password)
for line in cuisine.run("ps aux | grep island.webapp")[:-1].split("\n"):
	field = re.split("\s+", line)
	pid   = field[1]
	cuisine.run("sudo kill -9 {0} ; true".format(pid))

assert cuisine.run("echo OK"), "Cannot establish connection to host {0} with user {1} and password {2}".format(host, user, password)
with cuisine.mode_sudo():
	cuisine.file_upload("/etc/init.d/", os.path.join(SCRIPTS_PATH, "files/etc/init.d/island"))
	cuisine.run("chmod +x /etc/init.d/island")
	cuisine.run("update-rc.d island defaults")
	cuisine.run("/etc/init.d/island start")
# EOF
